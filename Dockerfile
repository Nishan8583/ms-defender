FROM debian:buster-slim
LABEL maintainer "TD"

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      git \
      python3 python3-pip python3-dev python3-setuptools \
      ca-certificates cargo libssl-dev \
      gettext \
      procps \
    && apt-get update \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# Place the startup wrapper script.
#ADD docker-entrypoint /usr/local/bin/
#RUN apt-get install build-essential libssl-dev libffi-dev \
#    python3-dev cargo -y
COPY requirements.txt .
RUN pip3 install -r requirements.txt

RUN groupadd --gid 1100 ms-defender && \
    useradd --uid 1100 --gid 1100 \
    -m --home-dir /opt/ms-defender ms-defender

COPY *.py /opt/ms-defender/

RUN chmod -R 0755 /opt/ms-defender && \
    mkdir /opt/ms-defender/tdBase && \
    chown -R ms-defender:ms-defender /opt/ms-defender && \
    mkdir /state

WORKDIR /opt/ms-defender
CMD ["python3", "-u", "/opt/ms-defender/ms-defender.py"]
