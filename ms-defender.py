#!/usr/bin/python
#__author
import requests, base64, json, time, logging, os,datetime,sys
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.transport import BeatsTransport
from logstash_async.handler import LogstashFormatter

# ms_defender is main class to get defender logs info from MS
# and then push it to logstash
class ms_defender():

    # provide the essential environments
    def __init__(self):

        # setting up environment key and value pair so that try and except block can be simplified
        self.envionment_vars = {"CLIENT_ID":"","TENANT_ID":"",
        "CLIENT_SECRET":"","LOGSTASH_IP":"","LOGSTASH_PORT":"",
        "QUERY_INTERVAL":"30",
         "TENANT":"","MSP":""}
        print("[*] Setting up environment varaibles")
        self.setup_environment()

        # setting beats output
        print("[*] Setting up beats output")
        self.setup_beats_output()


        # setting up URL
        self.api_url = "https://api.security.microsoft.com/api/"
        self.token_url = "https://login.microsoftonline.com/%s/oauth2/v2.0/token"%self.envionment_vars["TENANT_ID"]
        self.header = {"Content-Type": "application/json","Accept": "application/json",}
        
    # setup_environment sets up the necessary environment variables
    def setup_environment(self):

        for keys,values in self.envionment_vars.items():
            try:
                self.envionment_vars[keys] =  os.environ[keys]
            except Exception as e:
                print("FATAL Could not get environment variable {}, please set it ".format(keys))
                sys.exit(-1)
    
        self.time_format = "%Y-%m-%dT%H:%M:%SZ"# 2021-09-26T07:48:53.967+0545

    # setting up beats output
    def setup_beats_output(self):
        self.logstash = logging.getLogger('ms-defender-logger')
        self.logstash.setLevel(logging.INFO)

        transport = BeatsTransport(
                   self.envionment_vars["LOGSTASH_IP"],
                   int(self.envionment_vars["LOGSTASH_PORT"]),
                    timeout=5.0,
                    ssl_enable=False,
                    ssl_verify=False,
                    keyfile="",
                    certfile="",
                    ca_certs="",
                    )
        #Add extra LS fields
        extra_ls_fields = {}
        extra_ls_fields["event"] = {}
        extra_ls_fields["event"]["module"] = "ms-defender"
        extra_ls_fields["tenant"] = self.envionment_vars["TENANT"]
        extra_ls_fields["tenant"] = self.envionment_vars["MSP"]


        # doc says second parameter is not necessary, but if i remove this, error occurs
        formatter = LogstashFormatter(extra_prefix=None, extra=extra_ls_fields, message_type="ms-defender")
        handler = AsynchronousLogstashHandler(
            self.envionment_vars["LOGSTASH_IP"],
            int(self.envionment_vars["LOGSTASH_PORT"]),
            transport=transport,
            database_path='/state/{}-ms-defender-logstash.db'.format(self.envionment_vars["TENANT"]),
            )
        handler.setFormatter(formatter)
        self.logstash.addHandler(handler)

    def get_token(self):
        
        payload = "client_id={}&client_secret={}&grant_type=client_credentials&scope={}".format(
        self.envionment_vars["CLIENT_ID"],
        self.envionment_vars["CLIENT_SECRET"],
        "https://api.security.microsoft.com/.default"

        )
        try:
            token = requests.post(self.token_url,data=payload)
            token = token.json()["access_token"]
            self.header = {"Authorization":"Bearer %s"%token}
        except Exception as e:
            print("Could not request token due to error",e)

    
    # function for getting issue created
    def get_data(self):
        self.get_token()
        url = ""
        new_state = False
        try:
            with open("./state.json","r") as f:
                print("previous state was detected")
            time = datetime.datetime.now() - datetime.timedelta(minutes=int(self.envionment_vars["QUERY_INTERVAL"]))
            time = time.strftime(self.time_format)
            url = self.api_url+"incidents?$filter=createdTime gt %s"%time
        except:
            print("new query from beginning")
            url = self.api_url+"incidents"
            new_state = True
        try:
            response = requests.get(url,headers=self.header)
            res = response.json()
            values = res["value"]
            if new_state:
                with open("/state/state.json","w") as f:
                    print("creating new state")
                    f.write("1")
            return values
        except Exception as e:
            print("Could not get data due to error",e)
        

    
    # continous_query continues in a loop
    def continous_query(self):
        start = 0
        print("[+] Starting querying")
        while True:
            v = self.get_data()
            if v == None:
                print("[-] No data so sleeping")
            self.push_to_logstash(v)
    

    def push_to_logstash(self,msg_list):
        if msg_list == None:
            return
        if len(msg_list) == 0:
            return
        for values in msg_list:
            print("pushing data")
            values["event"] = {}
            values["event"]["module"] = "ms-defender"
            values["tenant"] = self.envionment_vars["TENANT"]
            values["tenant"] = self.envionment_vars["MSP"]
            self.logstash.info(values)

def main():

    j = ms_defender()

    #j = ms_defender("https://nishan-test-2.atlassian.net/rest/api/2/search/","nishan","lyLhbAcOGNf6dXcvTzz2DE26")
    j.continous_query()
    #get_state_json_test()

main()

