#!/usr/bin/python3
import yaml
import os
from yaml.loader import SafeLoader


#Modify Below elastic config:
C_LOGSTASH_IP="10.222.1.23"
C_LOGSTASH_PORT="5185"
IMAGE_NAME="ms_defender_app"

#config = open('365.json', 'r').read().splitlines()
with open('config.yml') as f:
    config = yaml.load(f, Loader=SafeLoader)


def generate_compose(tenant, msp, client_id, client_secret, tenant_id):
 template: str = f"""
 ms-defender-beat-{tenant}:
   image: {IMAGE_NAME}
   container_name: ms-defender-beat-{tenant}
   environment:
    - TENANT={tenant}
    - MSP={msp}
    - LOGSTASH_PORT={C_LOGSTASH_PORT}
    - LOGSTASH_IP={C_LOGSTASH_IP}
    - CLIENT_ID={client_id}
    - CLIENT_SECRET={client_secret}
    - TENANT_ID={tenant_id}
    - QUERY_INTERVAL=30
 
   volumes:
    - "/tenant/{tenant}/ms-defender/data:/state/"
   restart: always

"""
 write_to_compose(template)

def generate_compose_custom(tenant, msp, label):
 template: str = f"""
 azure-eventhub-beat-{tenant}-custom-{label}:
   image: {IMAGE_NAME}
   container_name: azure-eventhub-beat-{tenant}-custom
   environment:
    - TENANT={tenant}
    - MSP={msp}
    - LOGSTASH_PORT={C_LOGSTASH_PORT}
    - LOGSTASH_IP={C_LOGSTASH_IP}
   volumes:
    - /tenant/{tenant}/ms-defender-beat-custom-{label}/filebeat.yml:/filebeat/filebeat.yml
    - /tenant/{tenant}/ms-defender-beat-custom-{label}/data:/filebeat/data
   restart: always

"""
 write_to_compose(template)


def write_to_compose(text):
  with open("docker-compose.yml", "a") as compose:
      compose.write(text)

def check_file_exists(tenant):
  fbymlpath = f"/tenant/{tenant}/ms-defender-beat/filebeat.yml"
  if not os.path.exists(fbymlpath):
    os.makedirs(os.path.dirname(fbymlpath))
    with open(fbymlpath, "w") as yml:
      yml.write("\n")

with open("docker-compose.yml", "w") as new_compose:
  header = "version: \'3.7\'\nservices:\n"
  new_compose.write(header)


for i in config:
  print(i)
  try:
    #IF MSP key does not exist in config, just mirror tenant
      try:
          if config[i]["msp"]:
              pass
      except:
          config[i]["msp"] = config[i]["tenant"]
  except:
      print("error")

  if config[i].get("custom", "false") == True:
      #Generate custom config which mounts a custom filebeat config
      try:
        print("trying to generate compose file with custom filebeat config")
        check_file_exists(config[i]["tenant"])
        generate_compose_custom(config[i]["tenant"], config[i]["msp"], i)
      except Exception as e:
        print("Could not generate custom config for filebeat due to error=",e,"Continuing with next one")
        continue
  #else:
  try:
    client_id = config[i]["client_id"]
    client_secret = config[i]["client_secret"]
    tenant_id = config[i]["tenant_id"]
  except:
    print("Could not get necessary vars from config, so using asterik instead")
    client_id = "****"
    client_secret = "****"
    tenant_id = "****"

      #Generate Standard Config
  generate_compose(config[i]["tenant"], config[i]["msp"], client_id, client_secret, tenant_id)

